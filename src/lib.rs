mod types;
pub mod messages;

pub use types::message::Serialize as Serde;

#[cfg(test)]
mod tests {
    use crate::messages::{Ping, LightningMessage};
    use crate::types::message::Serialize;

    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }

    #[test]
    fn codec_works() {
        let message = Ping {
            num_pong_bytes: 290,
            byteslen: Some(4),
            ignored: vec![0, 0, 0, 0],
        };
        let serialization = message.serialize();
        println!("extension: {:?}", serialization);
        let deserialization = Ping::parse(&serialization);
        println!("recovery: {:#?}", deserialization);
        assert_eq!(deserialization.num_pong_bytes, 290);
        assert_eq!(deserialization.ignored.len(), 4);
        receive_lightning_message(LightningMessage::Ping(*deserialization));
    }

    pub fn receive_lightning_message(message: LightningMessage) {
        match message {
            LightningMessage::Ping(ping) => {
                println!("It's a ping message! {:?}", ping);
            }
            LightningMessage::Pong(pong) => {
                println!("It's a pong message! {:?}", pong);
            }
        };
    }
}
