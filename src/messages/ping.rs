use crate::types::message::Serialize;
use crate::types::field::LightningMessageType;

#[derive(Debug)]
pub struct PingMessage {
	pub num_pong_bytes: u16,
	pub byteslen: Option<u16>,
	pub ignored: Vec<u8>,
}

impl Serialize for PingMessage {
    fn placeholder_field_array() -> Vec<LightningMessageType> {
        vec![
            LightningMessageType::Int16(0),
            LightningMessageType::LengthAnnotatedBuffer(0, Vec::new())
        ]
    }

    fn to_field_array(&self) -> Vec<LightningMessageType> {
        let mut fields = Vec::new();
        fields.push(LightningMessageType::Int16(self.num_pong_bytes));
        fields.push(LightningMessageType::LengthAnnotatedBuffer(self.ignored.len() as u16, self.ignored.clone()));
        fields
    }

	fn from_field_array(fields: &[LightningMessageType]) -> Box<Self> {
		let num_pong_bytes = fields[0].int_16_value().unwrap();
		let ignored = fields[1].length_annotated_buffer_value().unwrap();

		Box::new(PingMessage {
			num_pong_bytes,
			byteslen: Some(ignored.len() as u16),
			ignored,
		})
	}

}