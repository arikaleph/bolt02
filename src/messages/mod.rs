mod ping;
mod pong;

pub use ping::PingMessage as Ping;
pub use pong::PongMessage as Pong;

pub enum LightningMessage {
    Ping(Ping),
    Pong(Pong),
}